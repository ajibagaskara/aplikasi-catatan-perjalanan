<?php 
  
  if(isset($_POST["submit"]) and $_POST["submit"]=="login"){
      
      require_once("models/User.php");
      $user = new User();

      $nik = $_POST['nik'];
      $nm_lengkap = $_POST['nm_lengkap'];

      if(empty($nik) AND empty($nm_lengkap)){
        header("Location:".BASE_URL."index.php?m=2");
        exit();
      }else{
        $result = $user->login($nik, $nm_lengkap);
        $get = $result->fetch_object();
        session_start();
        if(isset($get)){
          $_SESSION["id_user"]=$get->id_user;
          $_SESSION["nik"]=$get->nik;
          $_SESSION["nm_lengkap"]=$get->nm_lengkap;
          header("Location:".BASE_URL."view/perjalanan/");
          exit();
        }else{
          header("Location:".BASE_URL."index.php?m=1");
          exit();
        }
      }
  }
?>

<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Aplikasi Catatan Perjalan | Log in</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="public/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="public/bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="public/bower_components/Ionicons/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="public/dist/css/AdminLTE.min.css">
  <!-- iCheck -->
  <link rel="stylesheet" href="public/plugins/iCheck/square/blue.css">

  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition login-page">
<div class="login-box">
  <div class="login-logo">
    <a href="#"><b>Peduli</b>Diri</a>
  </div>
  <!-- /.login-logo -->
  <div class="login-box-body">
    <p class="login-box-msg">Silahkan Masukan NIK & Nama Lengkap Anda</p>

    <!-- Notification -->
    <?php
      require 'view/layout_partial/alert.php';
    ?>

    <form method="post">
      <div class="form-group has-feedback">
        <input type="text" class="form-control" placeholder="NIK" name="nik">
        <span class="glyphicon glyphicon-user form-control-feedback"></span>
      </div>
      <div class="form-group has-feedback">
        <input type="text" class="form-control" placeholder="Nama Lengkap" name="nm_lengkap">
        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
      </div>
      <div class="row">
        <div class="col-xs-8">
            <a href="" class="btn btn-info btn-block btn-flat">
              <i class="fa fa-user"></i> <span>Daftar Pengguna Baru</span>
            </a>
        </div>
        <div class="col-xs-4">
            <input type="hidden" name="submit" value="login">
            <button type="submit" class="btn btn-primary btn-block btn-flat">Masuk</button>
        </div>
        <!-- /.col -->
      </div>
    </form>
  </div>
  <!-- /.login-box-body -->
</div>
<!-- /.login-box -->

<!-- jQuery 3 -->
<script src="public/bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="public/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

</body>
</html>
